#include <iostream>
#include <vector>
#include <string.h>

using namespace std;


bool charIsDigit(char anyChar) {
    return int(anyChar) >= 48 && int(anyChar) <= 57;
}


vector<char *> *split(char *expression, char delimiter) {
    auto *splitted = new vector<char *>;

    char *buffer = nullptr;

    char bufferSize = 2;

    char bufferIndex = 0;

    while ('\0' != *expression) {


        if (buffer == nullptr) {
            buffer = (char *) malloc(sizeof(char) * bufferSize);
        }

        buffer = (char *) realloc(buffer, sizeof(char) * ++bufferSize);

        buffer[bufferIndex++] = *expression++;


        if (*expression == '\0' || (*expression == delimiter && expression++)) {


            buffer[++bufferIndex] = '\0';

            splitted->push_back(buffer);

            buffer = nullptr;
            bufferIndex = 0;
        }

    }

    return splitted;
}


void echoNumericOnly(const vector<char *> &strings) {
    for (char *currentString : strings) {
        if (charIsDigit(currentString[0]) && charIsDigit(currentString[strlen(currentString) - 1])) {
            cout << currentString << endl;
        }
    }
}


int main() {

    echoNumericOnly(*split("1a3 abc ", ' '));

}